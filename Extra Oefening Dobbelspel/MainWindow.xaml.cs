﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Extra_Oefening_Dobbelspel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        public int playerScore;
        public int computerScore;
        public bool rad1Check;
        public bool rad2Check;
        public bool rad3Check;
        public void EindeSpel()
        {

        }
        private void DobbelenRnd()
        {
            Random rnd = new Random();
            int getal1;
            int getal2;
            getal1 = rnd.Next(1, 7);
            getal2 = rnd.Next(1, 7);

            if (getal1 == getal2)
            {
                TxtResultaat.Text = "Gelijk";
            }
            else if (getal1 < getal2)
            {
                TxtResultaat.Text = "Computer";
                computerScore++;
            }
            else if (getal1 > getal2)
            {
                TxtResultaat.Text = "Player";
                playerScore++;
            }
            TxtPlayer.Text += $"{getal1}\n";
            TxtComputer.Text += $"{getal2}\n";
            LblPlayer.Content = $"{playerScore} keer gewonnen";
            LblComputer.Content = $"{computerScore} keer gewonnen";
        }



        private void BtnDobbelen_Click(object sender, RoutedEventArgs e)
        {
            if(rad1Check)
            {
                DobbelenRnd();
                if (playerScore == 5)
                {
                    rad1Check = false;
                    TxtResultaat.Text = "Player wint";
                    TxtResultaat.Background = Brushes.Green;
                }
                else if (computerScore == 5)
                {
                    rad1Check = false;
                    TxtResultaat.Text = "Computer wint";
                    TxtResultaat.Background = Brushes.Red;
                }

            }

            else if (rad2Check)
            {
                DobbelenRnd();
                if (playerScore == 10)
                {
                    rad2Check = false;
                    TxtResultaat.Text = "Player wint";
                    TxtResultaat.Background = Brushes.Green;
                }
                else if (computerScore == 10)
                {
                    rad2Check = false;
                    TxtResultaat.Text = "Computer wint";
                    TxtResultaat.Background = Brushes.Red;
                }

            }

            else if (rad3Check)
            {
                DobbelenRnd();
                if (playerScore == 15)
                {
                    rad3Check = false;
                    TxtResultaat.Text = "Player wint";
                    TxtResultaat.Background = Brushes.Green;
                }
                else if (computerScore == 15)
                {
                    rad3Check = false;
                    TxtResultaat.Text = "Computer wint";
                    TxtResultaat.Background = Brushes.Red;
                }

            }
        }

        public void Rad1_Checked(object sender, RoutedEventArgs e)
        {
            rad1Check = true;
            rad2Check = false;
            rad3Check = false;
            BtnDobbelen.IsEnabled = true;
        }

        private void Rad2_Checked(object sender, RoutedEventArgs e)
        {
            rad1Check = false;
            rad2Check = true;
            rad3Check = false;
            BtnDobbelen.IsEnabled = true;
        }

        private void Rad3_Checked(object sender, RoutedEventArgs e)
        {
            rad1Check = false;
            rad2Check = false;
            rad3Check = true;
            BtnDobbelen.IsEnabled = true;
        }
    }
}
